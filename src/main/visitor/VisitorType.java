package main.visitor;
import main.ast.node.*;
import main.ast.node.Program;
import main.ast.node.declaration.*;
import main.ast.node.declaration.handler.*;
import main.ast.node.declaration.VarDeclaration;
import main.ast.node.expression.*;
import main.ast.node.expression.operators.BinaryOperator;
import main.ast.node.expression.operators.UnaryOperator;
import main.ast.node.expression.values.BooleanValue;
import main.ast.node.expression.values.IntValue;
import main.ast.node.expression.values.StringValue;
import main.ast.node.statement.*;
import main.ast.type.Type;
import main.ast.type.actorType.ActorType;
import main.ast.type.arrayType.ArrayType;
import main.ast.type.noType.NoType;
import main.ast.type.primitiveType.BooleanType;
import main.ast.type.primitiveType.IntType;
import main.ast.type.primitiveType.StringType;
import main.compileError.CompileErrorException;
import main.symbolTable.*;
import main.symbolTable.itemException.ItemNotFoundException;
import main.symbolTable.symbolTableVariableItem.SymbolTableVariableItem;

import java.util.ArrayList;

public class VisitorType implements Visitor {
    private int numOfFors = 0;
    private boolean isInitial = false;
    private ActorDeclaration currentActorDeclaration = null;
    private HandlerDeclaration currentHandlerDeclaration = null;
    private HandlerDeclaration foundHandlerDeclaration = null;
    private ActorVarAccess currentVarAccess = null;

    public int isSubtype(String actorName, String ancestor)
    {
        SymbolTableActorItem cursor = null;
        try {
            cursor = (SymbolTableActorItem) SymbolTable.root.get("Actor_" + actorName);
        } catch (ItemNotFoundException e) {}
        while (cursor != null && cursor.getParentName() != null)
        {
            SymbolTableItem Item = null;
            if(cursor.getParentName().equals(ancestor))
                return 1;
            try {
                Item = SymbolTable.root.get("Actor_" + cursor.getParentName());
            } catch (ItemNotFoundException e) {}
            SymbolTableActorItem parent = (SymbolTableActorItem) Item;
            cursor = parent;
        }
        return 0;
    }

    public int parentsHave(String actorName, String itemName)
    {
        ArrayList<String> seen = new ArrayList<String>();
        SymbolTableActorItem cursor = null;
        try {
            cursor = (SymbolTableActorItem) SymbolTable.root.get("Actor_" + actorName);
        } catch (ItemNotFoundException e) {
        }
        if(cursor != null)
            while (cursor.getParentName() != null && !cursor.getParentName().equals(actorName))
            {
                for(int i = 0; i < seen.size(); i++)
                    if(seen.get(i).equals(cursor.getParentName()))
                        return 0;
                seen.add(cursor.getParentName());

                SymbolTableItem Item = null;
                try {
                    Item = SymbolTable.root.get("Actor_" + cursor.getParentName());
                } catch (ItemNotFoundException e) {

                }


                SymbolTableActorItem parent = (SymbolTableActorItem) Item;
                if(parent == null)
                    return 0;
                try {
                    parent.getActorSymbolTable().get(itemName);
                    if(itemName.contains("Handler_"))
                        foundHandlerDeclaration =
                                ((SymbolTableHandlerItem)parent.getActorSymbolTable().get(itemName)).getHandlerDeclaration();
                } catch (ItemNotFoundException e) {
                    cursor = parent;
                    continue;
                }
                return 1;
            }
        return 0;
    }

    protected void visitStatement( Statement stat )
    {
        if( stat == null )
            return;
        else if( stat instanceof MsgHandlerCall )
            this.visit( ( MsgHandlerCall ) stat );
        else if( stat instanceof Block )
            this.visit( ( Block ) stat );
        else if( stat instanceof Conditional )
            this.visit( ( Conditional ) stat );
        else if( stat instanceof For )
            this.visit( ( For ) stat );
        else if( stat instanceof Break )
            this.visit( ( Break ) stat );
        else if( stat instanceof Continue )
            this.visit( ( Continue ) stat );
        else if( stat instanceof Print )
            this.visit( ( Print ) stat );
        else if( stat instanceof Assign )
            this.visit( ( Assign ) stat );
    }

    protected void visitExpr( Expression expr )
    {
        if( expr == null )
            return;
        else if( expr instanceof UnaryExpression )
            this.visit( ( UnaryExpression ) expr );
        else if( expr instanceof BinaryExpression )
            this.visit( ( BinaryExpression ) expr );
        else if( expr instanceof ArrayCall )
            this.visit( ( ArrayCall ) expr );
        else if( expr instanceof ActorVarAccess )
            this.visit( ( ActorVarAccess ) expr );
        else if( expr instanceof Identifier )
            this.visit( ( Identifier ) expr );
        else if( expr instanceof Self )
            this.visit( ( Self ) expr );
        else if( expr instanceof Sender )
            this.visit( ( Sender ) expr );
        else if( expr instanceof BooleanValue )
            this.visit( ( BooleanValue ) expr );
        else if( expr instanceof IntValue )
            this.visit( ( IntValue ) expr );
        else if( expr instanceof StringValue )
            this.visit( ( StringValue ) expr );
    }

    @Override
    public void visit(Program program) {
        ArrayList<ActorDeclaration> actors = program.getActors();
        if(actors != null)
            for(int i = 0; i < actors.size(); i++)
            {
                actors.get(i).accept(this);
            }
        Main main = program.getMain();
            if(main != null)
            main.accept(this);
    }

    @Override
    public void visit(ActorDeclaration actorDeclaration) {
        currentActorDeclaration = actorDeclaration;
        if(actorDeclaration != null)
            actorDeclaration.getName().accept(this);
        Identifier parent = (actorDeclaration != null)? actorDeclaration.getParentName() : null;
        if(parent != null) {
            parent.accept(this);
            try {
                SymbolTable.root.get("Actor_" + parent.getName());
            } catch (ItemNotFoundException e) {
                System.out.println("Line:" + Integer.toString(actorDeclaration.getLine()) +
                        ":actor " + parent.getName() + " is not declared");
            }
        }
        ArrayList<VarDeclaration> knownActors = actorDeclaration.getKnownActors();
        if(knownActors != null)
            for(int i = 0; i < knownActors.size(); i++)
            {
                knownActors.get(i).accept(this);
                try {
                    SymbolTable.root.get("Actor_" + knownActors.get(i).getType());
                } catch (ItemNotFoundException e) {
                    System.out.println("Line:" + Integer.toString(knownActors.get(i).getLine()) +
                            ":actor " + knownActors.get(i).getType() + " is not declared");
                }
            }
        ArrayList<VarDeclaration> actorVars = actorDeclaration.getActorVars();
        if(actorVars != null)
            for(int i = 0; i < actorVars.size(); i++)
            {
                actorVars.get(i).accept(this);;
            }
        InitHandlerDeclaration init = actorDeclaration.getInitHandler();
        if(init != null) {
            isInitial = true;
            init.accept(this);
            isInitial = false;
        }
        ArrayList<MsgHandlerDeclaration> msgHandlers = actorDeclaration.getMsgHandlers();
        if(msgHandlers != null)
            for(int i = 0; i < msgHandlers.size(); i++)
            {
                msgHandlers.get(i).accept(this);;
            }
        currentActorDeclaration = null;
    }

    @Override
    public void visit(HandlerDeclaration handlerDeclaration) {
        currentHandlerDeclaration = handlerDeclaration;
        Identifier name = handlerDeclaration.getName();
        name.setType(new NoType());
        if(name != null)
            name.accept(this);
        ArrayList<VarDeclaration> args = handlerDeclaration.getArgs();
        if(args != null)
            for(int i = 0; i < args.size(); i++)
            {
                args.get(i).accept(this);;
            }
        ArrayList<VarDeclaration> localVars = handlerDeclaration.getLocalVars();
        if(localVars != null)
            for(int i = 0; i < localVars.size(); i++)
            {
                localVars.get(i).accept(this);;
            }
        ArrayList<Statement> body = handlerDeclaration.getBody();
        if(body != null)
            for(int i = 0; i < body.size(); i++)
            {
                this.visitStatement(body.get(i));
            }
        currentHandlerDeclaration = null;
    }

    @Override
    public void visit(VarDeclaration varDeclaration) {
        Identifier id = varDeclaration.getIdentifier();
        if(id != null)
            id.accept(this);
        varDeclaration.getIdentifier().setType(varDeclaration.getType());
    }

    @Override
    public void visit(Main mainActors) {
        ArrayList<ActorInstantiation> actors = mainActors.getMainActors();
        SymbolTableActorItem acItem = null;
        if(actors != null)
            for(int i = 0; i < actors.size(); i++)
            {
                boolean actorExists = true;
                actors.get(i).accept(this);
                try {
                    acItem = (SymbolTableActorItem) SymbolTable.root.get("Actor_" + actors.get(i).getType());
                } catch (ItemNotFoundException e) {
                    actorExists = false;
                    System.out.println("Line:" + Integer.toString(actors.get(i).getLine()) +
                            ":actor " + actors.get(i).getType() + " is not declared");
                }
                if(acItem != null && actorExists)
                {
                    if(actors.get(i).getKnownActors().size() != acItem.getActorDeclaration().getKnownActors().size())
                        System.out.println("Line:" + Integer.toString(actors.get(i).getLine()) +
                                ":knownactors does not match with definition");
                    else
                    {
                        for(int j = 0; j < actors.get(i).getKnownActors().size(); j++)
                        {
                            if(!(actors.get(i).getKnownActors().get(j).getType() instanceof NoType))
                            if(!(((ActorType)actors.get(i).getKnownActors().get(j).getType()).toString()
                                    .equals(((ActorType)acItem.getActorDeclaration().getKnownActors().get(j).getType()).toString()) ||
                                    isSubtype(((ActorType)actors.get(i).getKnownActors().get(j).getType()).toString(),
                                            ((ActorType)acItem.getActorDeclaration().getKnownActors().get(j).getType()).toString()) == 1))
                                System.out.println("Line:" + Integer.toString(actors.get(i).getLine()) +
                                        ":knownactors does not match with definition");
                        }
                    }

                    if(acItem.getActorDeclaration().getInitHandler() != null)
                    if(actors.get(i).getInitArgs().size() != acItem.getActorDeclaration().getInitHandler().getArgs().size())
                        System.out.println("Line:" + Integer.toString(actors.get(i).getLine()) +
                                ":arguments do not match with definition");
                    else
                    {
                        for(int j = 0; j < actors.get(i).getInitArgs().size(); j++)
                        {
                            if(!(actors.get(i).getInitArgs().get(j).getType() instanceof NoType ||
                                    acItem.getActorDeclaration().getInitHandler().getArgs().get(j).getType() instanceof NoType))
                            {
                                if(actors.get(i).getInitArgs().get(j).getType() != null)
                                if(!(actors.get(i).getInitArgs().get(j).getType().toString()
                                        .equals(acItem.getActorDeclaration().getInitHandler().getArgs().get(j).getType().toString())))
                                {
                                    System.out.println("Line:" + Integer.toString(actors.get(i).getLine()) +
                                            ":arguments do not match with definition");
                                }
                            }
                        }
                    }
                    if(acItem.getActorDeclaration().getInitHandler() == null &&
                            actors.get(i).getInitArgs().size() > 0)
                        System.out.println("Line:" + Integer.toString(actors.get(i).getLine()) +
                                ":arguments do not match with definition");
                }
            }
    }

    @Override
    public void visit(ActorInstantiation actorInstantiation) {
        Identifier id = actorInstantiation.getIdentifier();
        if(id != null) {
            id.setType(actorInstantiation.getType());
            id.accept(this);
        }
        ArrayList<Identifier> knownActors = actorInstantiation.getKnownActors();
        if(knownActors != null)
            for(int i = 0; i < knownActors.size(); i++)
            {
                knownActors.get(i).accept(this);
            }
        ArrayList<Expression> args = actorInstantiation.getInitArgs();
        if(args != null)
            for(int i = 0; i < args.size(); i++)
            {
                this.visitExpr(args.get(i));
            }
    }

    @Override
    public void visit(UnaryExpression unaryExpression) {
        Expression operand = unaryExpression.getOperand();
        UnaryOperator op = unaryExpression.getUnaryOperator();
        if(operand != null)
            this.visitExpr(operand);
        if(op == UnaryOperator.postdec || op == UnaryOperator.postinc || op == UnaryOperator.predec || op == UnaryOperator.preinc)
        if(!(unaryExpression.getOperand() instanceof Identifier || unaryExpression.getOperand() instanceof ArrayCall
        || unaryExpression.getOperand() instanceof ActorVarAccess)) {
            if(op == UnaryOperator.postdec || op == UnaryOperator.predec)
                System.out.println("Line:" + Integer.toString(unaryExpression.getLine()) +
                        ":lvalue required as decrement operand");
            else
                System.out.println("Line:" + Integer.toString(unaryExpression.getLine()) +
                        ":lvalue required as increment operand");
//            unaryExpression.getOperand().setType(new NoType());
            unaryExpression.setType(new NoType());
        }
        if(operand.getType() != null)
        if(unaryExpression.getUnaryOperator() == UnaryOperator.minus ||
                unaryExpression.getUnaryOperator() == UnaryOperator.preinc ||
                unaryExpression.getUnaryOperator() == UnaryOperator.predec ||
                unaryExpression.getUnaryOperator() == UnaryOperator.postinc ||
                unaryExpression.getUnaryOperator() == UnaryOperator.postdec)
        {
//            System.out.println(operand.getType().toString());
            if(!(operand.getType() instanceof IntType || operand.getType() instanceof NoType))
            {
                System.out.println("Line:" + Integer.toString(unaryExpression.getLine()) +
                        ":unsupported operand type for " + unaryExpression.getUnaryOperator().name());
                unaryExpression.setType(new NoType());
            }
            else if(operand.getType() instanceof NoType)
                unaryExpression.setType(new NoType());
            else
                unaryExpression.setType(new IntType());
        }
        else if(unaryExpression.getUnaryOperator() == UnaryOperator.not)
        {
            if(!(operand.getType() instanceof BooleanType || operand.getType() instanceof NoType))
            {
                System.out.println("Line:" + Integer.toString(unaryExpression.getLine()) +
                        ":unsupported operand type for " + unaryExpression.getUnaryOperator().name());
                unaryExpression.setType(new NoType());
            }
            else if(operand.getType() instanceof NoType)
                unaryExpression.setType(new NoType());
            else
                unaryExpression.setType(new BooleanType());
        }
    }

    @Override
    public void visit(BinaryExpression binaryExpression) {
        BinaryOperator op = binaryExpression.getBinaryOperator();
        Expression left = binaryExpression.getLeft();
        if(left != null)
            this.visitExpr(left);
        Expression right = binaryExpression.getRight();
        if(right != null)
            this.visitExpr(right);
        if(op == BinaryOperator.add || op == BinaryOperator.div || op == BinaryOperator.mod ||
                op == BinaryOperator.mult || op == BinaryOperator.sub)
            binaryExpression.setType(new IntType());
        else
            binaryExpression.setType(new BooleanType());
        if(left.getType() != null && right.getType() != null)
        if(op == BinaryOperator.assign)
        {
            if(!((right.getType() instanceof IntType && left.getType() instanceof IntType) ||
                    (right.getType() instanceof BooleanType && left.getType() instanceof BooleanType) ||
                    (right.getType() instanceof StringType && left.getType() instanceof StringType) ||
                    (right.getType() instanceof ArrayType && left.getType() instanceof ArrayType) ||
                    (right.getType() instanceof ActorType && left.getType() instanceof ActorType &&
                            isSubtype( ((ActorType) left.getType()).getName().getName(),
                                    ((ActorType) left.getType()).getName().getName()) == 1) ||
                    (right.getType() instanceof ActorType && left.getType() instanceof ActorType &&
                            ((ActorType)right.getType()).toString().equals("Sender")) ||
                            right.getType() instanceof NoType) || left.getType() instanceof NoType)
            {
                System.out.println("Line:" + Integer.toString(right.getLine()) + ":unsupported operand type for assign");
                binaryExpression.setType(new NoType());
            }
            else {
                binaryExpression.setType(left.getType());
            }
        }
        else if(op == BinaryOperator.add || op == BinaryOperator.div || op == BinaryOperator.mod ||
                op == BinaryOperator.mult || op == BinaryOperator.sub || op == BinaryOperator.gt ||
                op == BinaryOperator.lt)
        {
            if(!(left.getType() instanceof IntType || left.getType() instanceof NoType))
            {
                System.out.println("Line:" + Integer.toString(left.getLine()) +
                        ":unsupported operand type for " + op.name());
                binaryExpression.setType(new NoType());
            }
            else if(!(right.getType() instanceof IntType || right.getType() instanceof NoType))
            {
                System.out.println("Line:" + Integer.toString(right.getLine()) +
                ":unsupported operand type for " + op.name());
                binaryExpression.setType(new NoType());
            }
            else if(left.getType() instanceof NoType)
                binaryExpression.setType(new NoType());
            else if(right.getType() instanceof NoType)
                binaryExpression.setType(new NoType());
        }
        else if(op == BinaryOperator.and || op == BinaryOperator.or)
        {
            if(!(left.getType() instanceof BooleanType || left.getType() instanceof NoType))
            {
                System.out.println("Line:" + Integer.toString(left.getLine()) +
                        ":unsupported operand type for " + op.name());
                binaryExpression.setType(new NoType());
            }
            else if(!(right.getType() instanceof BooleanType || right.getType() instanceof NoType))
            {
                System.out.println("Line:" + Integer.toString(right.getLine()) +
                ":unsupported operand type for " + op.name());
                binaryExpression.setType(new NoType());
            }
            else if(left.getType() instanceof NoType)
                binaryExpression.setType(new NoType());
            else if(right.getType() instanceof NoType)
                binaryExpression.setType(new NoType());
        }
    }

    @Override
    public void visit(ArrayCall arrayCall) {
        Expression arrCall = arrayCall.getArrayInstance();
        if(arrCall != null)
            this.visitExpr(arrCall);
        Expression index = arrayCall.getIndex();
        if(index != null) {
            this.visitExpr(index);
        }
        if(!(index.getType() instanceof IntType || index.getType() instanceof NoType))
        {
            System.out.println("Line:" + Integer.toString(arrayCall.getLine()) +
                    ":array index not int");
        }
        arrayCall.setType(new IntType());
        SymbolTableActorItem acItem = null;
        SymbolTableHandlerItem handlerItem = null;
        SymbolTableVariableItem varItem = null;
        try {
            acItem = (SymbolTableActorItem) SymbolTable.root.get(currentActorDeclaration.getName().getName());
        } catch (ItemNotFoundException e) {return;}
        try {
            handlerItem = (SymbolTableHandlerItem) acItem.getActorSymbolTable().get("Handler_" + currentHandlerDeclaration.getName().getName());
        } catch (ItemNotFoundException e) {}
        try {
            varItem = (SymbolTableVariableItem) handlerItem.getHandlerSymbolTable()
                    .get("Variable_" + ((Identifier) arrayCall.getArrayInstance()).getName());
            if(!(varItem.getType() instanceof ArrayType || varItem.getType() instanceof NoType))
            {
                System.out.println("Line:" + Integer.toString(arrayCall.getLine()) +
                        ":array type not ArrayType");
                arrayCall.setType(new NoType());
            }
        } catch (ItemNotFoundException e) {
            System.out.println("Line:" + Integer.toString(arrayCall.getLine()) +
                    ":variable " + ((Identifier) arrayCall.getArrayInstance()).getName() + " is not declared");
        }
    }

    @Override
    public void visit(ActorVarAccess actorVarAccess) {
        currentVarAccess = actorVarAccess;
        Self self = actorVarAccess.getSelf();
        if(self != null)
            self.accept(this);
        Identifier id = actorVarAccess.getVariable();
        if(id != null) {
            id.setType(new NoType());
            id.accept(this);
        }
        SymbolTableActorItem acItem = null;
        SymbolTableVariableItem varItem = null;
        if(currentActorDeclaration != null)
        {
            ArrayList<VarDeclaration> vars = currentActorDeclaration.getActorVars();
            for(int i = 0; i < vars.size(); i++)
                if(vars.get(i).getIdentifier().getName().equals(id.getName()))
                    actorVarAccess.setType(vars.get(i).getType());
        }
        currentVarAccess = null;
    }

    @Override
    public void visit(Identifier identifier) {
        SymbolTableVariableItem varItem = null;
        if(identifier.getType() == null)
        {
            if(currentActorDeclaration == null)
            {
                SymbolTableMainItem mainItem = null;
                try {
                    mainItem = (SymbolTableMainItem) SymbolTable.root.get("Main_main");
                } catch (ItemNotFoundException e) {}
                try {
                    varItem = (SymbolTableVariableItem) mainItem.getMainSymbolTable().get("Variable_" + identifier.getName());
                    identifier.setType(varItem.getType());
                } catch (ItemNotFoundException e) {
                    System.out.println("Line:" + Integer.toString(identifier.getLine()) +
                            ":variable " + identifier.getName() + " is not declared");
                    identifier.setType(new NoType());
                }
            }
            else if(currentHandlerDeclaration != null)
            {
                ArrayList<VarDeclaration> vars = currentActorDeclaration.getKnownActors();
                for(int i = 0; i < vars.size(); i++)
                    if(vars.get(i).getIdentifier().getName().equals(identifier.getName()))
                    {
                        identifier.setType(vars.get(i).getType());
                    }

                SymbolTableActorItem acItem = null;
                SymbolTableHandlerItem handlerItem = null;
                try {
                    acItem = (SymbolTableActorItem) SymbolTable.root.get("Actor_" + currentActorDeclaration.getName().getName());
                } catch (ItemNotFoundException e) {}
                if(acItem != null)
                {
                    try {
                        handlerItem = (SymbolTableHandlerItem) acItem.getActorSymbolTable().get("Handler_" +
                                currentHandlerDeclaration.getName().getName());
                    } catch (ItemNotFoundException e) {}
                    try {
                        varItem = (SymbolTableVariableItem) handlerItem.getHandlerSymbolTable()
                                .get("Variable_" + identifier.getName());
                        identifier.setType(varItem.getType());
                    } catch (ItemNotFoundException e) {
                        System.out.println("Line:" + Integer.toString(identifier.getLine()) +
                                ":variable " + identifier.getName() + " is not declared");
                        if(identifier.getType() == null)
                            identifier.setType(new NoType());
                    }
                }
            }
        }
    }

    @Override
    public void visit(Self self) {
        ActorType type = null;
        if(currentActorDeclaration != null)
        {
            type = new ActorType(currentActorDeclaration.getName());
            self.setType(type);
        }
        else
        {
            if(currentVarAccess != null)
            System.out.println("Line:" + Integer.toString(currentVarAccess.getLine()) +
                    ":self doesn't refer to any actor");
            self.setType(new NoType());
        }
    }

    @Override
    public void visit(Sender sender) {
        if(isInitial)
        {
            System.out.println("Line:" + Integer.toString(sender.getLine()) +
                    ":no sender in initial msghandler");
            sender.setType(new NoType());
        }
        Identifier id = new Identifier("Sender");
        sender.setType(new ActorType(id));
        if(currentActorDeclaration == null)
        {
            System.out.println("Line:" + Integer.toString(sender.getLine()) +
                    ":sender in wrong location");
        }
    }

    @Override
    public void visit(BooleanValue value) {
        value.setType(new BooleanType());
    }

    @Override
    public void visit(IntValue value) {
        value.setType(new IntType());
    }

    @Override
    public void visit(StringValue value) {
        value.setType(new StringType());
    }

    @Override
    public void visit(Block block) {
        ArrayList<Statement> sts = block.getStatements();
        if(sts != null)
            for(int i = 0; i < sts.size(); i++)
            {
                this.visitStatement(sts.get(i));
            }
    }

    @Override
    public void visit(Conditional conditional) {
        Expression expr = conditional.getExpression();
        if(expr != null)
            this.visitExpr(expr);
        if(!(expr.getType() instanceof BooleanType || expr.getType() instanceof NoType)) {
            System.out.println("Line:" + Integer.toString(expr.getLine()) +
                    ":condition type must be Boolean");
            expr.setType(new NoType());
        }
        Statement thn = conditional.getThenBody();
        if(thn != null)
            this.visitStatement(thn);
        Statement els = conditional.getElseBody();
        if(els != null)
            this.visitStatement(els);
    }

    @Override
    public void visit(For loop) {
        numOfFors++;
        Assign init = loop.getInitialize();
        if(init != null)
            init.accept(this);
        Expression cond = loop.getCondition();
        if(cond != null) {
            this.visitExpr(cond);
        }
        if(cond != null)
        if(!(cond.getType() instanceof BooleanType || cond.getType() instanceof NoType)) {
            System.out.println("Line:" + Integer.toString(cond.getLine()) +
                    ":condition type must be Boolean");
            cond.setType(new NoType());
        }
        Assign up = loop.getUpdate();
        if(up != null)
            up.accept(this);
        Statement body = loop.getBody();
        if(body != null)
            this.visitStatement(body);
        numOfFors--;
    }

    @Override
    public void visit(Break breakLoop) {
        if(numOfFors <= 0)
        {
            System.out.println("Line:" + Integer.toString(breakLoop.getLine()) +
                    ":break statement not within loop");
        }
    }

    @Override
    public void visit(Continue continueLoop) {
        if(numOfFors <= 0)
        {
            System.out.println("Line:" + Integer.toString(continueLoop.getLine()) +
                    ":continue statement not within loop");
        }
    }

    @Override
    public void visit(MsgHandlerCall msgHandlerCall) {
        Expression inst = msgHandlerCall.getInstance();
        if(inst != null)
        {
            if(inst.getType() == null)
                inst.setType(new NoType());
            this.visitExpr(inst);
        }
        Identifier id = msgHandlerCall.getMsgHandlerName();
        if(id != null)
        {
            if(id.getType() == null)
                id.setType(new NoType());
            id.accept(this);
        }
        ArrayList<Expression> arrList = msgHandlerCall.getArgs();
        if(arrList != null)
            for(int i = 0; i < arrList.size(); i++)
            {
                this.visitExpr(arrList.get(i));
            }

        boolean isCallable = false;
        SymbolTableActorItem acItem = null;
        if(!(msgHandlerCall.getInstance().toString().equals("Sender") ||
                msgHandlerCall.getInstance().toString().equals("Self")))
        {
            ArrayList<VarDeclaration> knownAcs = currentActorDeclaration.getKnownActors();
            for(int i = 0; i < knownAcs.size(); i++)
            {
                if (((Identifier) msgHandlerCall.getInstance()).getName().
                        equals(knownAcs.get(i).getIdentifier().getName())) {
                    isCallable = true;
                    break;
                }
            }

            if(!isCallable)
            {
                System.out.println("Line:" + Integer.toString(msgHandlerCall.getLine()) +
                        ":variable " + ((Identifier) msgHandlerCall.getInstance()).getName() + " is not callable");
            }

        }
        acItem = null;
        SymbolTableVariableItem varItem = null;
        SymbolTableHandlerItem handlerItem = null;
        if(!(msgHandlerCall.getInstance().toString().equals("Sender")))
        {
            Identifier instanceName;
            if(msgHandlerCall.getInstance().toString().equals("Self"))
            {
                instanceName = currentActorDeclaration.getName();
                try {
                    acItem = (SymbolTableActorItem) SymbolTable.root.get("Actor_" + currentActorDeclaration.getName().getName());
                } catch (ItemNotFoundException e) {}
                try {
                    handlerItem = (SymbolTableHandlerItem) acItem.getActorSymbolTable()
                            .get("Handler_" + msgHandlerCall.getMsgHandlerName());
                    foundHandlerDeclaration = handlerItem.getHandlerDeclaration();
                } catch (ItemNotFoundException e) {
                    if(parentsHave(acItem.getActorDeclaration().getName().getName(),
                            "Handler_" + msgHandlerCall.getMsgHandlerName().getName()) == 0)
                        System.out.println("Line:" + Integer.toString(msgHandlerCall.getLine()) +
                                ":there is no msghandler name " + msgHandlerCall.getMsgHandlerName().getName() + " in actor " +
                                acItem.getActorDeclaration().getName().getName());
                }
                if(foundHandlerDeclaration != null)
                {
                    ArrayList<VarDeclaration> found = foundHandlerDeclaration.getArgs();
                    if(found.size() != msgHandlerCall.getArgs().size())
                        System.out.println("Line:" + Integer.toString(msgHandlerCall.getLine()) +
                                ":arguments do not match with definition");
                    else
                    {
                        for(int j = 0; j < found.size(); j++)
                        {
                            if(!(found.get(j).getType() instanceof NoType || msgHandlerCall.getArgs().get(j).getType() instanceof NoType))
                            {
                                if(!(found.get(j).getType().toString().equals(msgHandlerCall.getArgs().get(j).getType().toString())))
                                {
                                    System.out.println("Line:" + Integer.toString(msgHandlerCall.getArgs().get(j).getLine()) +
                                            ":arguments do not match with definition");
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                instanceName = (Identifier) msgHandlerCall.getInstance();
                ArrayList<VarDeclaration> vars = new ArrayList<VarDeclaration>();
                vars.addAll(currentHandlerDeclaration.getArgs());
                vars.addAll(currentHandlerDeclaration.getLocalVars());
                boolean isDefined = false;
                for(int i = 0; i < vars.size(); i++)
                    if(vars.get(i).getIdentifier().getName().equals(instanceName.getName()))
                    {
                        isDefined = true;
                        break;
                    }

                try {
                    acItem = (SymbolTableActorItem) SymbolTable.root.get("Actor_" + currentActorDeclaration.getName().getName());
                } catch (ItemNotFoundException e) {}
                try {
                    varItem = (SymbolTableVariableItem) acItem.getActorSymbolTable()
                            .get("Variable_" + instanceName.getName());
                } catch (ItemNotFoundException e) {
                    if(!isDefined)
                    System.out.println("Line:" + Integer.toString(msgHandlerCall.getLine()) +
                            ":variable " + instanceName.getName() + " is not declared");
                }
                if(varItem != null && (varItem.getType() instanceof ActorType))
                {
                    try {
                        acItem = (SymbolTableActorItem) SymbolTable.root
                                .get("Actor_" + ((ActorType) varItem.getType()).getName().getName());
                    } catch (ItemNotFoundException e) {
                        System.out.println("Line:" + Integer.toString(msgHandlerCall.getLine()) +
                                ":variable " + instanceName.getName() + " is not declared");
                    }
                    if(acItem != null)
                    {
                        try {
                            handlerItem = (SymbolTableHandlerItem) acItem.getActorSymbolTable()
                                    .get("Handler_" + msgHandlerCall.getMsgHandlerName().getName());
                            foundHandlerDeclaration = handlerItem.getHandlerDeclaration();
                        } catch (ItemNotFoundException e) {
                            if(parentsHave(acItem.getActorDeclaration().getName().getName(),
                                    "Handler_" + msgHandlerCall.getMsgHandlerName().getName()) == 0)
                                System.out.println("Line:" + Integer.toString(msgHandlerCall.getLine()) +
                                        ":there is no msghandler name " + msgHandlerCall.getMsgHandlerName().getName() + " in actor " +
                                        acItem.getActorDeclaration().getName().getName());
                        }
                        if(foundHandlerDeclaration != null)
                        {
                            ArrayList<VarDeclaration> found = foundHandlerDeclaration.getArgs();
                            if(found.size() != msgHandlerCall.getArgs().size())
                                System.out.println("Line:" + Integer.toString(msgHandlerCall.getLine()) +
                                        ":arguments do not match with definition");
                            else
                            {
                                for(int j = 0; j < found.size(); j++)
                                {
                                    if(!(found.get(j).getType() instanceof NoType || msgHandlerCall.getArgs().get(j).getType() instanceof NoType))
                                    {
                                        if(found.get(j).getType() != null && msgHandlerCall.getArgs().get(j).getType() != null)
                                        if(!(found.get(j).getType().toString().equals(msgHandlerCall.getArgs().get(j).getType().toString())))
                                        {
                                            System.out.println("Line:" + Integer.toString(msgHandlerCall.getArgs().get(j).getLine()) +
                                                    ":arguments do not match with definition");
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        foundHandlerDeclaration = null;
    }

    @Override
    public void visit(Print print) {
        Expression arg = print.getArg();
        if(arg != null)
            this.visitExpr(arg);
        if(!(arg.getType() instanceof IntType || arg.getType() instanceof BooleanType ||
                arg.getType() instanceof StringType || arg.getType() instanceof ArrayType ||
                arg.getType() instanceof NoType))
        {
            System.out.println("Line:" + Integer.toString(arg.getLine()) +
                            ":unsupported type for print");
            arg.setType(new NoType());
        }
    }

    @Override
    public void visit(Assign assign) {
        Expression lVal = assign.getlValue();
        if(lVal != null)
            this.visitExpr(lVal);
        Expression rVal = assign.getrValue();
        if(rVal != null)
            this.visitExpr(rVal);
        if(rVal.getType() != null && lVal.getType() != null)
        if(!((rVal.getType() instanceof IntType && lVal.getType() instanceof IntType) ||
                (rVal.getType() instanceof BooleanType && lVal.getType() instanceof BooleanType) ||
                (rVal.getType() instanceof StringType && lVal.getType() instanceof StringType) ||
                (rVal.getType() instanceof ArrayType && lVal.getType() instanceof ArrayType) ||
                (rVal.getType() instanceof ActorType && lVal.getType() instanceof ActorType &&
                isSubtype( ((ActorType) rVal.getType()).getName().getName(),
                        ((ActorType) lVal.getType()).getName().getName()) == 1) ||
                (rVal.getType() instanceof ActorType && lVal.getType() instanceof ActorType &&
                        ((ActorType)rVal.getType()).toString().equals("Sender")) ||
                rVal.getType() instanceof NoType || lVal.getType() instanceof NoType))
        {
            System.out.println("Line:" + Integer.toString(rVal.getLine()) + ":unsupported operand type for assign");
            lVal.setType(new NoType());
        }
        if(!(lVal instanceof Identifier || lVal instanceof ArrayCall
                || lVal instanceof ActorVarAccess)) {
            System.out.println("Line:" + Integer.toString(rVal.getLine()) +
                    ":left side of assignment must be a valid lvalue");
        }
        if(rVal.getType() instanceof ArrayType && lVal.getType() instanceof ArrayType)
        {
            int size1 = -1, size2 = -1;
            Identifier id1, id2;
            if(lVal instanceof ActorVarAccess)
                id1 = ((ActorVarAccess) lVal).getVariable();
            else if(lVal instanceof Identifier)
                id1 = (Identifier)lVal;
            else
                return;
            if(rVal instanceof ActorVarAccess)
                id2 = ((ActorVarAccess) rVal).getVariable();
            else if(rVal instanceof Identifier)
                id2 = (Identifier)rVal;
            else
                return;
            ArrayList<VarDeclaration> acVars = currentActorDeclaration.getActorVars();
            for(int i = 0; i < acVars.size(); i++)
            {
                if(acVars.get(i).getIdentifier().getName().equals(id1.getName()) &&
                acVars.get(i).getType() instanceof ArrayType)
                    size1 = ((ArrayType)acVars.get(i).getType()).getSize();
                if(acVars.get(i).getIdentifier().getName().equals(id2.getName()) &&
                        acVars.get(i).getType() instanceof ArrayType)
                    size2 = ((ArrayType)acVars.get(i).getType()).getSize();
            }
            acVars = new ArrayList<VarDeclaration>();
            acVars.addAll(currentHandlerDeclaration.getArgs());
            acVars.addAll(currentHandlerDeclaration.getLocalVars());
            for(int i = 0; i < acVars.size(); i++)
            {
                if(acVars.get(i).getIdentifier().getName().equals(id1.getName()) &&
                        acVars.get(i).getType() instanceof ArrayType)
                    size1 = ((ArrayType)acVars.get(i).getType()).getSize();
                if(acVars.get(i).getIdentifier().getName().equals(id2.getName()) &&
                        acVars.get(i).getType() instanceof ArrayType)
                    size2 = ((ArrayType)acVars.get(i).getType()).getSize();
            }
            if(size1 > 0 && size2 > 0)
            if(size1 != size2)
            System.out.println("Line:" + Integer.toString(rVal.getLine()) +
                    ":operation assign requires equal array sizes");
        }
    }
}
