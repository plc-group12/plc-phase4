package main.visitor;
import main.ast.node.*;
import main.ast.node.Program;
import main.ast.node.declaration.*;
import main.ast.node.declaration.handler.*;
import main.ast.node.declaration.VarDeclaration;
import main.ast.node.expression.*;
import main.ast.node.expression.operators.BinaryOperator;
import main.ast.node.expression.operators.UnaryOperator;
import main.ast.node.expression.values.BooleanValue;
import main.ast.node.expression.values.IntValue;
import main.ast.node.expression.values.StringValue;
import main.ast.node.statement.*;
import main.ast.type.Type;
import main.ast.type.actorType.ActorType;
import main.ast.type.arrayType.ArrayType;
import main.ast.type.primitiveType.BooleanType;
import main.ast.type.primitiveType.IntType;
import main.ast.type.primitiveType.StringType;
import main.symbolTable.SymbolTable;
import main.symbolTable.SymbolTableActorItem;
import main.symbolTable.SymbolTableHandlerItem;
import main.symbolTable.SymbolTableMainItem;
import main.symbolTable.symbolTableVariableItem.SymbolTableVariableItem;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class VisitorCodeGen implements Visitor {

    private FileWriter actorWriter, handlerWriter;
    private ActorDeclaration currentActorDeclaration;
    private HandlerDeclaration currentHandlerDeclaration;
    private InitHandlerDeclaration currentInit;
    private boolean islVal = false;
    private int labelNum = 0;
    private ArrayList<String> breakJumpLines = new ArrayList<String >();
    private ArrayList<String> continueJumpLines = new ArrayList<String >();

    public Void actorWrite(String code){
        try{
            this.actorWriter.write(code + "\n");
        }catch(Exception e){
            System.out.println("0- " + e);
        }
        return null;
    }

    public Void handlerWrite(String code){
        try{
            this.handlerWriter.write(code + "\n");
        }catch(Exception e){
            System.out.println("1- " + e);
        }
        return null;
    }

    public String getLabel()
    {
        labelNum++;
        return "LABEL" + labelNum;
    }

    public String getTypeAcronym(Type type)
    {
        if(type instanceof IntType)
            return "I";
        else if(type instanceof BooleanType)
            return "Z";
        else if(type instanceof StringType)
            return "Ljava/lang/String;";
        else if(type instanceof ArrayType)
            return "[I";
        else
            return "L" + ((ActorType) type).toString() + ";";
    }

    public String getArgsAcronyms(ArrayList<VarDeclaration> args)
    {
        String answer = "";
        if(args == null || args.size() == 0)
            return answer;
        for(int i = 0; i < args.size(); i++)
        {
            answer += getTypeAcronym(args.get(i).getType());
        }
        return answer;
    }

    public int getIndex(String varName)
    {
        try {
            if(currentInit != null)
            {
                ArrayList<VarDeclaration> args = currentInit.getArgs();
                ArrayList<VarDeclaration> locals = currentInit.getLocalVars();
                if(args.size() != 0)
                for(int i = 0; i < args.size(); i++)
                {
                    if(args.get(i).getIdentifier().getName().equals(varName))
                        return i + 1;
                }
                if(locals.size() != 0)
                for(int i = 0; i < locals.size(); i++)
                {
                    if(locals.get(i).getIdentifier().getName().equals(varName))
                        return i + 1 + args.size();
                }
                return -1;
            }
            else if(currentHandlerDeclaration != null)
            {
                ArrayList<VarDeclaration> args = currentHandlerDeclaration.getArgs();
                ArrayList<VarDeclaration> locals = currentHandlerDeclaration.getLocalVars();
                if(args.size() != 0)
                for(int i = 0; i < args.size(); i++)
                {
                    if(args.get(i).getIdentifier().getName().equals(varName))
                        return i + 2;
                }
                if(locals.size() != 0)
                for(int i = 0; i < locals.size(); i++)
                {
                    if(locals.get(i).getIdentifier().getName().equals(varName))
                        return i + 2 + args.size();
                }
                return -1;
            }
        } catch (Exception e) {
            System.out.println("2- " + e);
        }
        return -1;
    }

    public void saveVal(Expression var)
    {
        String name = "";
        boolean isActorVar = false;
        Type type = null;
        if(var instanceof Identifier)
        {
            name = ((Identifier)var).getName();
        }
        else if(var instanceof ArrayCall)
        {
            this.actorWrite("iastore");
            return;
        }
        else if(var instanceof ActorVarAccess)
        {
            name = ((ActorVarAccess)var).getVariable().getName();
            isActorVar = true;
        }
        else
            System.out.println("Mistake in using saveVal function!"); // To be removed!
        int index = getIndex(name);
        if(index != -1 && !isActorVar)
        {
            if(var.getType() instanceof IntType || var.getType() instanceof BooleanType)
                this.actorWrite("istore " + index);
            else
            {
                this.actorWrite("astore " + index);
            }
        }
        else
        {
            this.actorWrite("aload_0");
            this.actorWrite("dup_x1");
            this.actorWrite("pop");
            this.actorWrite("putfield " + currentActorDeclaration.getName().getName() + "/" + name +
                    " " + getTypeAcronym(var.getType()));
        }
    }

    public void loadVal(Expression var)
    {
        String name = "";
        boolean isActorVar = false;
        Type type = null;
        if(var instanceof Identifier)
        {
            name = ((Identifier)var).getName();
        }
        else if(var instanceof ArrayCall)
        {
            this.actorWrite("iaload");
            return;
        }
        else if(var instanceof ActorVarAccess)
        {
            name = ((ActorVarAccess)var).getVariable().getName();
            isActorVar = true;
        }
        else
            System.out.println("Mistake in using loadVal function!"); // To be removed!///////////////////////////////
        int index = getIndex(name);
        if(index != -1 && !isActorVar)
        {
            if(var.getType() instanceof IntType || var.getType() instanceof BooleanType)
                this.actorWrite("iload " + index);
            else
            {
                this.actorWrite("aload " + index);
            }
        }
        else
        {
            this.actorWrite("aload_0");
            this.actorWrite("getfield " + currentActorDeclaration.getName().getName() + "/" + name +
                    " " + getTypeAcronym(var.getType()));
        }
    }

    protected void visitStatement( Statement stat )
    {
        if( stat == null )
            return;
        else if( stat instanceof MsgHandlerCall )
            this.visit( ( MsgHandlerCall ) stat );
        else if( stat instanceof Block )
            this.visit( ( Block ) stat );
        else if( stat instanceof Conditional )
            this.visit( ( Conditional ) stat );
        else if( stat instanceof For )
            this.visit( ( For ) stat );
        else if( stat instanceof Break )
            this.visit( ( Break ) stat );
        else if( stat instanceof Continue )
            this.visit( ( Continue ) stat );
        else if( stat instanceof Print )
            this.visit( ( Print ) stat );
        else if( stat instanceof Assign )
            this.visit( ( Assign ) stat );
    }

    protected void visitExpr( Expression expr )
    {
        if( expr == null )
            return;
        else if( expr instanceof UnaryExpression )
            this.visit( ( UnaryExpression ) expr );
        else if( expr instanceof BinaryExpression )
            this.visit( ( BinaryExpression ) expr );
        else if( expr instanceof ArrayCall )
            this.visit( ( ArrayCall ) expr );
        else if( expr instanceof ActorVarAccess )
            this.visit( ( ActorVarAccess ) expr );
        else if( expr instanceof Identifier )
            this.visit( ( Identifier ) expr );
        else if( expr instanceof Self )
            this.visit( ( Self ) expr );
        else if( expr instanceof Sender )
            this.visit( ( Sender ) expr );
        else if( expr instanceof BooleanValue )
            this.visit( ( BooleanValue ) expr );
        else if( expr instanceof IntValue )
            this.visit( ( IntValue ) expr );
        else if( expr instanceof StringValue )
            this.visit( ( StringValue ) expr );
    }

    @Override
    public void visit(Program program) {
        currentActorDeclaration = null;
        currentHandlerDeclaration = null;
        currentInit = null;
        ArrayList<ActorDeclaration> actors = program.getActors();
        if(actors != null)
            for(int i = 0; i < actors.size(); i++)
            {
                currentActorDeclaration = actors.get(i);
                actors.get(i).accept(this);
                currentActorDeclaration = null;
            }
        Main main = program.getMain();
        if(main != null)
            main.accept(this);
        ///////////////////////////////////////////////
        try {
            File file = new File("output/DefaultActor.j");
            file.createNewFile();
            this.handlerWriter = new FileWriter(file);
            this.handlerWrite(".class public DefaultActor");
            this.handlerWrite(".super java/lang/Thread");
            this.handlerWrite(".method public <init>()V");
            this.handlerWrite(".limit stack 1000");
            this.handlerWrite(".limit locals 1000");
            this.handlerWrite("aload_0");
            this.handlerWrite("invokespecial java/lang/Thread/<init>()V");
            this.handlerWrite("return");
            this.handlerWrite(".end method");
            Set<String> methodSigns = new HashSet<String>();
            if(actors != null)
                for(int i = 0; i < actors.size(); i++)
                {
                    ArrayList<MsgHandlerDeclaration> handlers = actors.get(i).getMsgHandlers();
                    for(int j = 0; j < handlers.size(); j++)
                    {
                        String sign = "send_";
                        sign += handlers.get(j).getName().getName() + "(LActor;" +
                                getArgsAcronyms(handlers.get(j).getArgs()) + ")V";
                        methodSigns.add(sign);
                    }
                }
            for(String str : methodSigns)
            {
                String name = str.substring(5, str.indexOf("("));
                this.handlerWrite(".method public " + str);
                this.handlerWrite(".limit stack 1000");
                this.handlerWrite(".limit locals 1000");
                this.handlerWrite("getstatic java/lang/System/out Ljava/io/PrintStream;");
                this.handlerWrite("ldc \"there is no msghandler named " + name + " in sender\"");
                this.handlerWrite("invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
                this.handlerWrite("return");
                this.handlerWrite(".end method");
            }
            this.handlerWriter.close();
        }
        catch (Exception e) {
            System.out.println("3- " + e);
        }
    }

    @Override
    public void visit(ActorDeclaration actorDeclaration) {
        try{
            File file = new File("output/" + actorDeclaration.getName().getName() + ".j");
            file.createNewFile();
            this.actorWriter = new FileWriter(file);
            this.actorWrite(".class public " + actorDeclaration.getName().getName());
            this.actorWrite(".super Actor");
            ArrayList<VarDeclaration> knownActors = actorDeclaration.getKnownActors();
            if(knownActors != null)
                for(int i = 0; i < knownActors.size(); i++)
                {
                    this.actorWrite(".field " + knownActors.get(i).getIdentifier().getName() + " " +
                            getTypeAcronym(knownActors.get(i).getType()));
                }
            ArrayList<VarDeclaration> actorVars = actorDeclaration.getActorVars();
            if(actorVars != null)
                for(int i = 0; i < actorVars.size(); i++)
                {
                    this.actorWrite(".field " + actorVars.get(i).getIdentifier().getName() + " " +
                            getTypeAcronym(actorVars.get(i).getType()));
                }
            this.actorWrite(".method public <init>(I)V");
            this.actorWrite(".limit stack 1000");
            this.actorWrite(".limit locals 1000");
            this.actorWrite("aload_0");
            this.actorWrite("iload_1");
            this.actorWrite("invokespecial Actor/<init>(I)V");
            if(actorVars != null)
                for(int i = 0; i < actorVars.size(); i++)
                {
                    actorVars.get(i).accept(this);
                }
            this.actorWrite("return");
            this.actorWrite(".end method");

            InitHandlerDeclaration init = actorDeclaration.getInitHandler();
            currentInit = init;
            if(init != null) {
                this.actorWrite(".method public initial(" + getArgsAcronyms(init.getArgs()) + ")V");
                this.actorWrite(".limit stack 1000");
                this.actorWrite(".limit locals 1000");

                ArrayList<VarDeclaration> localVars = init.getLocalVars();
                if(localVars != null)
                    for(int i = 0; i < localVars.size(); i++)
                    {
                        localVars.get(i).accept(this);
                    }
                ArrayList<Statement> body = init.getBody();
                if(body != null)
                    for(int i = 0; i < body.size(); i++)
                    {
                        body.get(i).accept(this);
                    }

                this.actorWrite("return");
                this.actorWrite(".end method");
            }
            currentInit = null;

            ArrayList<VarDeclaration> acts = currentActorDeclaration.getKnownActors();
            this.actorWrite(".method public setKnownActors(" + getArgsAcronyms(acts) + ")V");
            this.actorWrite(".limit stack 1000");
            this.actorWrite(".limit locals 1000");
            for(int i = 0; i < acts.size(); i++)
            {
                this.actorWrite("aload_0");
                this.actorWrite("aload " + Integer.toString(i + 1));
                this.actorWrite("putfield " + currentActorDeclaration.getName().getName() + "/" +
                        acts.get(i).getIdentifier().getName() + " " + getTypeAcronym(acts.get(i).getType()));
            }
            this.actorWrite("return");
            this.actorWrite(".end method");


            ArrayList<MsgHandlerDeclaration> msgHandlers = actorDeclaration.getMsgHandlers();
            if(msgHandlers != null)
                for(int i = 0; i < msgHandlers.size(); i++)
                {
                    currentHandlerDeclaration = msgHandlers.get(i);
                    msgHandlers.get(i).accept(this);
                    currentHandlerDeclaration = null;
                }
            this.actorWriter.close();

        }catch(Exception e){
            System.out.println("4- " + e);
        }
    }

    @Override
    public void visit(HandlerDeclaration handlerDeclaration) {
        try{
            File file = new File("output/" + currentActorDeclaration.getName().getName() + "_" +
                    handlerDeclaration.getName().getName() + ".j");
            file.createNewFile();
            this.handlerWriter = new FileWriter(file);
            this.handlerWrite(".class public " + currentActorDeclaration.getName().getName() + "_" +
                    handlerDeclaration.getName().getName());
            this.handlerWrite(".super Message");
            ArrayList<VarDeclaration> args = handlerDeclaration.getArgs();
            if(args != null)
                for(int i = 0; i < args.size(); i++)
                {
                    this.handlerWrite(".field private " + args.get(i).getIdentifier().getName() + " " +
                            getTypeAcronym(args.get(i).getType()));
                }

            this.handlerWrite(".field private receiver L" + currentActorDeclaration.getName().getName() + ";");
            this.handlerWrite(".field private sender LActor;");

            this.handlerWrite(".method public <init>(L" + currentActorDeclaration.getName().getName() + ";LActor;" +
                    getArgsAcronyms(currentHandlerDeclaration.getArgs()) + ")V");
            this.handlerWrite(".limit stack 1000");
            this.handlerWrite(".limit locals 1000");
            this.handlerWrite("aload_0");
            this.handlerWrite("invokespecial Message/<init>()V");
            this.handlerWrite("aload_0");
            this.handlerWrite("aload_1");
            this.handlerWrite("putfield " + currentActorDeclaration.getName().getName() + "_" +
                    handlerDeclaration.getName().getName() + "/receiver L" + currentActorDeclaration.getName().getName() + ";");
            this.handlerWrite("aload_0");
            this.handlerWrite("aload_2");
            this.handlerWrite("putfield " + currentActorDeclaration.getName().getName() + "_" +
                    handlerDeclaration.getName().getName() + "/sender LActor;");
            if(args != null)
                for(int i = 0; i < args.size(); i++)
                {
                    this.handlerWrite("aload_0");
                    if(args.get(i).getType() instanceof IntType)
                        this.handlerWrite("iload " + Integer.toString(3 + i));
                    else
                        this.handlerWrite("aload " + Integer.toString(3 + i));
                    this.handlerWrite("putfield " + currentActorDeclaration.getName().getName() + "_" +
                            handlerDeclaration.getName().getName() + "/" + args.get(i).getIdentifier().getName() +
                            " " + getTypeAcronym(args.get(i).getType()));
                }
            this.handlerWrite("return");
            this.handlerWrite(".end method");
            this.handlerWrite(".method public execute()V");
            this.handlerWrite(".limit stack 1000");
            this.handlerWrite(".limit locals 1000");
            this.handlerWrite("aload_0");
            this.handlerWrite("getfield " + currentActorDeclaration.getName().getName() + "_" +
                    handlerDeclaration.getName().getName() + "/receiver L" + currentActorDeclaration.getName().getName() + ";");
            this.handlerWrite("aload_0");
            this.handlerWrite("getfield " + currentActorDeclaration.getName().getName() + "_" +
                    handlerDeclaration.getName().getName() + "/sender LActor;");
            for(int i = 0; i < args.size(); i++)
            {
                this.handlerWrite("aload_0");
                this.handlerWrite("getfield " + currentActorDeclaration.getName().getName() + "_" +
                        handlerDeclaration.getName().getName() + "/" + args.get(i).getIdentifier().getName() +
                        " " + getTypeAcronym(args.get(i).getType()));
            }
            this.handlerWrite("invokevirtual " + currentActorDeclaration.getName().getName() + "/" +
                    currentHandlerDeclaration.getName().getName() + "(LActor;" + getArgsAcronyms(args) + ")V");
            this.handlerWrite("return");
            this.handlerWrite(".end method");

            this.handlerWriter.close();
            ///////////////////////////////////
            this.actorWrite(".method public send_" + currentHandlerDeclaration.getName().getName() + "(LActor;" +
                    getArgsAcronyms(args) + ")V");
            this.actorWrite(".limit stack 1000");
            this.actorWrite(".limit locals 1000");
            this.actorWrite("aload_0");
            this.actorWrite("new " + currentActorDeclaration.getName().getName() + "_" +
                    currentHandlerDeclaration.getName().getName());
            this.actorWrite("dup");
            this.actorWrite("aload_0");
            this.actorWrite("aload_1");
            for(int i = 0; i < args.size(); i++)
            {
                if(args.get(i).getType() instanceof IntType)
                    this.actorWrite("iload "+ Integer.toString(i + 2));
                else
                    this.actorWrite("aload " + Integer.toString(i + 2));
            }
            this.actorWrite("invokespecial " + currentActorDeclaration.getName().getName() + "_" +
                    currentHandlerDeclaration.getName().getName() + "/<init>(L" + currentActorDeclaration.getName().getName() +
                    ";LActor;" + getArgsAcronyms(args) + ")V");
            this.actorWrite("invokevirtual " + currentActorDeclaration.getName().getName() + "/send(LMessage;)V");
            this.actorWrite("return");
            this.actorWrite(".end method");

            this.actorWrite(".method public " + currentHandlerDeclaration.getName().getName() + "(LActor;" +
                    getArgsAcronyms(args) + ")V");
            this.actorWrite(".limit stack 1000");
            this.actorWrite(".limit locals 1000");

            ArrayList<VarDeclaration> localVars = handlerDeclaration.getLocalVars();
            if(localVars != null)
                for(int i = 0; i < localVars.size(); i++)
                {
                    localVars.get(i).accept(this);
                }
            ArrayList<Statement> body = handlerDeclaration.getBody();
            if(body != null)
                for(int i = 0; i < body.size(); i++)
                {
                    body.get(i).accept(this);
                }
            this.actorWrite("return");
            this.actorWrite(".end method");

        }catch(Exception e){
            System.out.println("5- " + e);
        }
    }

    @Override
    public void visit(VarDeclaration varDeclaration) { // Complete
        Identifier id = varDeclaration.getIdentifier();
//        if(id != null)
//            id.accept(this);
        if(varDeclaration.getType() instanceof ArrayType)
        {
            int index = getIndex(varDeclaration.getIdentifier().getName());
            if(index != -1)
            {
                this.actorWrite("bipush " + ((ArrayType)varDeclaration.getType()).getSize());
                this.actorWrite("newarray int");
                this.actorWrite("astore " + index);
            }
            else
            {
                this.actorWrite("aload_0");
                this.actorWrite("bipush " + ((ArrayType)varDeclaration.getType()).getSize());
                this.actorWrite("newarray int");
                this.actorWrite("putfield " + currentActorDeclaration.getName().getName() + "/" + id.getName() +
                        " [I");
            }
        }
    }

    @Override
    public void visit(Main mainActors) {
        try {
            File file = new File("output/Main.j");
            file.createNewFile();
            this.handlerWriter = new FileWriter(file);
            actorWriter = handlerWriter;
            this.handlerWrite(".class public Main");
            this.handlerWrite(".super java/lang/Object");
            this.handlerWrite(".method public <init>()V");
            this.handlerWrite(".limit stack 1000");
            this.handlerWrite(".limit locals 1000");
            this.handlerWrite("aload_0");
            this.handlerWrite("invokespecial java/lang/Object/<init>()V");
            this.handlerWrite("return");
            this.handlerWrite(".end method");
            this.handlerWrite(".method public static main([Ljava/lang/String;)V");
            this.handlerWrite(".limit stack 1000");
            this.handlerWrite(".limit locals 1000");
            ArrayList<ActorInstantiation> acIns = mainActors.getMainActors();
            for(int i = 0; i < acIns.size(); i++)
            {
                this.handlerWrite("new " + ((ActorType)acIns.get(i).getType()).getName().getName());
                this.handlerWrite("dup");
                this.handlerWrite("bipush " + ((SymbolTableActorItem)SymbolTable.root
                        .get("Actor_" + ((ActorType)acIns.get(i).getType()).getName().getName()))
                        .getActorDeclaration().getQueueSize());
                this.handlerWrite("invokespecial " +
                        ((ActorType)acIns.get(i).getType()).getName().getName() + "/<init>(I)V");
                this.handlerWrite("astore " + Integer.toString(i + 1));
            }
            for(int i = 0; i < acIns.size(); i++)
            {
                this.handlerWrite("aload " + Integer.toString(i + 1));
                ArrayList<Identifier> acs = acIns.get(i).getKnownActors();
                ArrayList<Type> types = new ArrayList<Type>();
                for(int q = 0; q < acs.size(); q++)
                    for(int j = 0; j < acIns.size(); j++)
                        if(acs.get(q).getName().equals(acIns.get(j).getIdentifier().getName()))
                        {
                            this.handlerWrite("aload " + Integer.toString(j + 1));
                            types.add(acs.get(q).getType());
                            break;
                        }
                String typesStr = "";
                for(int j = 0; j < types.size(); j++)
                    typesStr += getTypeAcronym(types.get(j));
                this.handlerWrite("invokevirtual " + ((ActorType)acIns.get(i).getType()).getName().getName()
                        + "/setKnownActors(" + typesStr + ")V");
            }
            for(int i = 0; i < acIns.size(); i++)
            {
                if(((SymbolTableActorItem)SymbolTable.root.get("Actor_" +
                        ((ActorType)acIns.get(i).getType()).getName().getName())).getActorDeclaration()
                        .getInitHandler() != null)
                {
                    String types = "";
                    this.handlerWrite("aload " + Integer.toString(i + 1));
                    for(int j = 0; j < acIns.get(i).getInitArgs().size(); j++)
                    {
                        types += getTypeAcronym(acIns.get(i).getInitArgs().get(j).getType());
                        acIns.get(i).getInitArgs().get(j).accept(this);
                    }
                    this.handlerWrite("invokevirtual " + ((ActorType)acIns.get(i).getType()).getName().getName()
                            + "/initial(" + types +")V");
                }
            }
            for(int i = 0; i < acIns.size(); i++)
            {
                this.handlerWrite("aload " + Integer.toString(i + 1));
                this.handlerWrite("invokevirtual " + ((ActorType)acIns.get(i).getType()).getName().getName()
                        + "/start()V");
            }
            this.handlerWrite("return");
            this.handlerWrite(".end method");

            this.handlerWriter.close();
        }catch(Exception e){
            System.out.println("6- " + e);
        }
    }

    @Override
    public void visit(ActorInstantiation actorInstantiation) { // Complete
    }


    @Override
    public void visit(UnaryExpression unaryExpression) { // Complete
        Expression operand = unaryExpression.getOperand();
        UnaryOperator op = unaryExpression.getUnaryOperator();
        if(operand != null)
            operand.accept(this);

        if(op == UnaryOperator.minus)
        {
            this.actorWrite("ineg");
        }
        else if(op == UnaryOperator.not)
        {
            this.actorWrite("iconst_1");
            this.actorWrite("ixor");
        }
        else if(op == UnaryOperator.postdec)
        {
            this.actorWrite("dup");
            this.actorWrite("iconst_1");
            this.actorWrite("isub");
            saveVal(operand);
        }
        else if(op == UnaryOperator.predec)
        {
            this.actorWrite("iconst_1");
            this.actorWrite("isub");
            this.actorWrite("dup");
            saveVal(operand);
        }
        else if(op == UnaryOperator.postinc)
        {
            this.actorWrite("dup");
            this.actorWrite("iconst_1");
            this.actorWrite("iadd");
            saveVal(operand);
        }
        else if(op == UnaryOperator.preinc)
        {
            this.actorWrite("iconst_1");
            this.actorWrite("iadd");
            this.actorWrite("dup");
            saveVal(operand);
        }
    }

    @Override
    public void visit(BinaryExpression binaryExpression) { // Complete
        BinaryOperator op = binaryExpression.getBinaryOperator();
        Expression left = binaryExpression.getLeft();
        Expression right = binaryExpression.getRight();
        if(left == null || right == null)
        {
            System.out.println("Operand null, expression abort!");
            return;
        }

        if(op == BinaryOperator.add)
        {
            left.accept(this);
            right.accept(this);
            this.actorWrite("iadd");
        }
        else if(op == BinaryOperator.sub)
        {
            left.accept(this);
            right.accept(this);
            this.actorWrite("isub");
        }
        else if(op == BinaryOperator.mult)
        {
            left.accept(this);
            right.accept(this);
            this.actorWrite("imult");
        }
        else if(op == BinaryOperator.div)
        {
            left.accept(this);
            right.accept(this);
            this.actorWrite("idiv");
        }
        else if(op == BinaryOperator.mod)
        {
            left.accept(this);
            right.accept(this);
            this.actorWrite("irem");
        }
        else if(op == BinaryOperator.and)
        {
            left.accept(this);
            String label1 = getLabel(), label2 = getLabel();
            this.actorWrite("ifeq " + label1);
            right.accept(this);
            this.actorWrite("ifeq " + label1);
            this.actorWrite("iconst_1");
            this.actorWrite("goto " + label2);
            this.actorWrite(label1 + ": iconst_0");
            this.actorWrite(label2 + ": ");
        }
        else if(op == BinaryOperator.or)
        {
            left.accept(this);
            String label1 = getLabel(), label2 = getLabel();
            this.actorWrite("ifne " + label1);
            right.accept(this);
            this.actorWrite("ifne " + label1);
            this.actorWrite("iconst_0");
            this.actorWrite("goto " + label2);
            this.actorWrite(label1 + ": iconst_1");
            this.actorWrite(label2 + ": ");
        }
        else if(op == BinaryOperator.assign)
        {
            Assign asgn = new Assign(left, right);
            asgn.accept(this);
            right.accept(this);
        }
        else if(op == BinaryOperator.eq)
        {
            left.accept(this);
            right.accept(this);
            if(left.getType() instanceof IntType || left.getType() instanceof BooleanType)
            {
                String label1 = getLabel(), label2 = getLabel();
                this.actorWrite("if_icmpeq " + label1);
                this.actorWrite("iconst_0");
                this.actorWrite("goto " + label2);
                this.actorWrite(label1 + ": iconst_1");
                this.actorWrite(label2 + ": ");
            }
            else
            {
                if(left.getType() instanceof ArrayType)
                    this.actorWrite("invokestatic java/util/Arrays.equals([I[I)Z");
                else
                    this.actorWrite("invokevirtual java/lang/Object.equals(Ljava/lang/Object;)Z");
            }
        }
        else if(op == BinaryOperator.neq)
        {
            left.accept(this);
            right.accept(this);
            if(left.getType() instanceof IntType || left.getType() instanceof BooleanType)
            {
                String label1 = getLabel(), label2 = getLabel();
                this.actorWrite("if_icmpne " + label1);
                this.actorWrite("iconst_0");
                this.actorWrite("goto " + label2);
                this.actorWrite(label1 + ": iconst_1");
                this.actorWrite(label2 + ": ");
            }
            else
            {
                if(left.getType() instanceof ArrayType)
                    this.actorWrite("invokestatic java/util/Arrays.equals([I[I)Z");
                else
                    this.actorWrite("invokevirtual java/lang/Object.equals(Ljava/lang/Object;)Z");
                this.actorWrite("iconst_1");
                this.actorWrite("ixor");
            }
        }
        else if(op == BinaryOperator.gt)
        {
            left.accept(this);
            right.accept(this);
            String label1 = getLabel(), label2 = getLabel();
            this.actorWrite("if_icmpgt " + label1);
            this.actorWrite("iconst_0");
            this.actorWrite("goto " + label2);
            this.actorWrite(label1 + ": iconst_1");
            this.actorWrite(label2 + ": ");
        }
        else if(op == BinaryOperator.lt)
        {
            left.accept(this);
            right.accept(this);
            String label1 = getLabel(), label2 = getLabel();
            this.actorWrite("if_icmplt " + label1);
            this.actorWrite("iconst_0");
            this.actorWrite("goto " + label2);
            this.actorWrite(label1 + ": iconst_1");
            this.actorWrite(label2 + ": ");
        }
    }

    @Override
    public void visit(ArrayCall arrayCall) { // Complete
        try {
            String name = "";
            if(arrayCall.getArrayInstance() instanceof Identifier)
                name = ((Identifier)arrayCall.getArrayInstance()).getName();
            else if(arrayCall.getArrayInstance() instanceof ActorVarAccess)
                name = ((ActorVarAccess)arrayCall.getArrayInstance()).getVariable().getName();
            int varIndex = getIndex(name);
            if(varIndex != -1)
            {
                this.actorWrite("aload " + varIndex);
            }
            else
            {
                this.actorWrite("aload_0");
                this.actorWrite("getfield " + currentActorDeclaration.getName().getName() + "/" + name +
                        " [I");
            }
            Expression index = arrayCall.getIndex();
            if (index != null)
                index.accept(this);
            if(!islVal)
                loadVal(arrayCall);
        } catch (Exception e) {
            System.out.println("7- " + e);
        }
    }

    @Override
    public void visit(ActorVarAccess actorVarAccess) {
        Self self = actorVarAccess.getSelf();
        Identifier id = actorVarAccess.getVariable();
        if(!islVal)
        {
            this.actorWrite("aload_0");
            this.actorWrite("getfield " + currentActorDeclaration.getName().getName() + "/" + id.getName() +
                    " " + getTypeAcronym(id.getType()));
        }
    }

    @Override
    public void visit(Identifier identifier) { // Complete
        if(!islVal)
        {
            loadVal(identifier);
        }
    }

    @Override
    public void visit(Self self) { // Complete
        this.actorWrite("aload_0");
    }

    @Override
    public void visit(Sender sender) { // Complete
        this.actorWrite("aload_1");
    }

    @Override
    public void visit(BooleanValue value) { // Complete
        if(value.getConstant())
            this.actorWrite("iconst_1");
        else
            this.actorWrite("iconst_0");
    }

    @Override
    public void visit(IntValue value) { // Complete
        this.actorWrite("bipush " + value.getConstant());
    }

    @Override
    public void visit(StringValue value) { // Complete
        this.actorWrite("ldc " + value.getConstant());
    }

    @Override
    public void visit(Block block) {
        ArrayList<Statement> sts = block.getStatements();
        if(sts != null)
            for(int i = 0; i < sts.size(); i++)
            {
                sts.get(i).accept(this);
            }
    }

    @Override
    public void visit(Conditional conditional) { // Complete
        Expression expr = conditional.getExpression();
        if(expr != null)
            expr.accept(this);
        String label1 = getLabel(), label2 = getLabel();
        this.actorWrite("ifeq " + label1);
        Statement thn = conditional.getThenBody();
        if(thn != null)
            thn.accept(this);
        this.actorWrite("goto " + label2);
        this.actorWrite(label1 + ": ");
        Statement els = conditional.getElseBody();
        if(els != null)
            els.accept(this);
        this.actorWrite(label2 + ": ");
    }

    @Override
    public void visit(For loop) { // Complete
        Assign init = loop.getInitialize();
        if(init != null)
            init.accept(this);
        Expression cond = loop.getCondition();
        if(cond != null)
            cond.accept(this);
        String label1 = getLabel(), label2 = getLabel(), label3 = getLabel();
        this.actorWrite(label2 + "ifeq " + label1);
        breakJumpLines.add(label1);
        continueJumpLines.add(label3);
        Statement body = loop.getBody();
        if(body != null)
            body.accept(this);
        Assign up = loop.getUpdate();
        this.actorWrite(label3 + ": ");
        if(up != null)
            up.accept(this);
        this.actorWrite("goto " + label2);
        this.actorWrite(label1 + ": ");
        if(breakJumpLines.get(breakJumpLines.size() - 1).equals(label1))
        {
            breakJumpLines.remove(breakJumpLines.size() - 1);
            continueJumpLines.remove(continueJumpLines.size() - 1);
        }
    }

    @Override
    public void visit(Break breakLoop) { // Complete
        if(breakJumpLines.size() > 0)
        {
            this.actorWrite("goto " + breakJumpLines.get(breakJumpLines.size() - 1));
            breakJumpLines.remove(breakJumpLines.size() - 1);
            continueJumpLines.remove(continueJumpLines.size() - 1);
        }
    }

    @Override
    public void visit(Continue continueLoop) { // Complete
        if(continueJumpLines.size() > 0)
        {
            this.actorWrite("goto " + continueJumpLines.get(continueJumpLines.size() - 1));
        }
    }

    @Override
    public void visit(MsgHandlerCall msgHandlerCall) { // Complete
        try {
            Expression inst = msgHandlerCall.getInstance();
            ArrayList<VarDeclaration> acs = currentActorDeclaration.getKnownActors();
            int knownActor = -1;
            String typesStr = "";
            SymbolTableActorItem actorItem = null;
            SymbolTableHandlerItem handItem = null;
            ArrayList<VarDeclaration> args;
            if(inst instanceof Identifier)
            {
                for (int q = 0; q < acs.size(); q++)
                    if (acs.get(q).getIdentifier().getName().equals(((Identifier) inst).getName())) {
                        this.actorWrite("aload_0");
                        this.actorWrite("getfield " + currentActorDeclaration.getName().getName() + "/" +
                                ((Identifier)inst).getName() + " " + getTypeAcronym(acs.get(q).getType()));
                        knownActor = q;
                        break;
                    }
            }
            else
            {
                inst.accept(this);
            }
            this.actorWrite("aload_0");
            ArrayList<Expression> arrList = msgHandlerCall.getArgs();
            if(arrList != null)
            {
                for(int i = 0; i < arrList.size(); i++)
                    arrList.get(i).accept(this);
            }
            typesStr += "LActor;";
            if(knownActor != -1)
            {
                actorItem = (SymbolTableActorItem)SymbolTable.root
                        .get("Actor_" + ((ActorType) acs.get(knownActor).getType()).getName().getName());
                handItem = (SymbolTableHandlerItem) actorItem.getActorSymbolTable()
                        .get("Handler_" + msgHandlerCall.getMsgHandlerName().getName());
                HandlerDeclaration handler = handItem.getHandlerDeclaration();
                args = handler.getArgs();
                if(args != null)
                    typesStr += getArgsAcronyms(args);
                this.actorWrite("invokevirtual " + ((ActorType) acs.get(knownActor).getType()).getName().getName()
                        + "/send_" + msgHandlerCall.getMsgHandlerName().getName() + "(" + typesStr + ")V");
            }
            else if(inst instanceof Sender)
            {
                for(int i = 0; i < msgHandlerCall.getArgs().size(); i++)
                {
                    typesStr += getTypeAcronym(msgHandlerCall.getArgs().get(i).getType());
                }
                this.actorWrite("invokevirtual Actor/send_" + msgHandlerCall.getMsgHandlerName().getName() +
                        "(" + typesStr + ")V");
            }
            else if(inst instanceof Self)
            {
                for(int i = 0; i < msgHandlerCall.getArgs().size(); i++)
                {
                    typesStr += getTypeAcronym(msgHandlerCall.getArgs().get(i).getType());
                }
                this.actorWrite("invokevirtual " + currentActorDeclaration.getName().getName() +
                        "/send_" + msgHandlerCall.getMsgHandlerName().getName() + "(" + typesStr + ")V");
            }
            else
            {
                System.out.println("Error in method call!"); // To be removed! ///////////////////////////////////
            }
        }
        catch (Exception e) {
            System.out.println("8- " + e);
        }
    }

    @Override
    public void visit(Print print) { // Complete
        this.actorWrite("getstatic java/lang/System/out Ljava/io/PrintStream;");
        Expression arg = print.getArg();
        if(arg != null)
            arg.accept(this);
        this.actorWrite("invokevirtual java/io/PrintStream/println(" + getTypeAcronym(arg.getType()) +")V");
    }

        @Override
    public void visit(Assign assign) { // Complete
        try {
            Expression lVal = assign.getlValue();
            if (lVal != null && lVal instanceof ArrayCall) {
                islVal = true;
                lVal.accept(this);
                islVal = false;
            }
            Expression rVal = assign.getrValue();
            if (rVal != null)
                rVal.accept(this);
            saveVal(lVal);
        } catch(Exception e) {
            System.out.println("9- " + e);
        }
    }
}
