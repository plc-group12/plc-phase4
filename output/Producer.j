.class public Producer
.super Actor
.field buffer LBufferManager;
.method public <init>(I)V
.limit stack 1000
.limit locals 1000
aload 0
iload 1
invokespecial Actor/<init>(I)V
return
.end method
.method public initial()V
.limit stack 1000
.limit locals 1000
aload 0
aload 0
invokevirtual Producer/send_beginProduce(LActor;)V
return
.end method
.method public setKnownActors(LBufferManager;)V
.limit stack 1000
.limit locals 1000
aload 0
aload 1
putfield Producer/buffer LBufferManager;
return
.end method
.method public send_produce(LActor;)V
.limit stack 1000
.limit locals 1000
aload 0
new Producer_produce
dup
aload 0
aload 1
invokespecial Producer_produce/<init>(LProducer;LActor;)V
invokevirtual Producer/send(LMessage;)V
return
.end method
.method public produce(LActor;)V
.limit stack 1000
.limit locals 1000
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "producer is producing"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
getfield Producer/buffer LBufferManager;
aload 0
invokevirtual BufferManager/send_ackProduce(LActor;)V
aload 0
aload 0
invokevirtual Producer/send_beginProduce(LActor;)V
return
.end method
.method public send_beginProduce(LActor;)V
.limit stack 1000
.limit locals 1000
aload 0
new Producer_beginProduce
dup
aload 0
aload 1
invokespecial Producer_beginProduce/<init>(LProducer;LActor;)V
invokevirtual Producer/send(LMessage;)V
return
.end method
.method public beginProduce(LActor;)V
.limit stack 1000
.limit locals 1000
aload 0
getfield Producer/buffer LBufferManager;
aload 0
invokevirtual BufferManager/send_giveMeNextProduce(LActor;)V
return
.end method
