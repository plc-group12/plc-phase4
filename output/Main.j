.class public Main
.super java/lang/Object
.method public <init>()V
.limit stack 1000
.limit locals 1000
aload 0
invokespecial java/lang/Object/<init>()V
return
.end method
.method public static main([Ljava/lang/String;)V
.limit stack 1000
.limit locals 1000
new BufferManager
dup
bipush 4
invokespecial BufferManager/<init>(I)V
astore 1
new Producer
dup
bipush 2
invokespecial Producer/<init>(I)V
astore 2
new Consumer
dup
bipush 2
invokespecial Consumer/<init>(I)V
astore 3
aload 1
aload 2
aload 3
invokevirtual BufferManager/setKnownActors(LProducer;LConsumer;)V
aload 2
aload 1
invokevirtual Producer/setKnownActors(LBufferManager;)V
aload 3
aload 1
invokevirtual Consumer/setKnownActors(LBufferManager;)V
aload 1
invokevirtual BufferManager/initial()V
aload 2
invokevirtual Producer/initial()V
aload 3
invokevirtual Consumer/initial()V
aload 1
invokevirtual BufferManager/start()V
aload 2
invokevirtual Producer/start()V
aload 3
invokevirtual Consumer/start()V
return
.end method
