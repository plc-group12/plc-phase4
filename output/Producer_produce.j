.class public Producer_produce
.super Message
.field private receiver LProducer;
.field private sender LActor;
.method public <init>(LProducer;LActor;)V
.limit stack 1000
.limit locals 1000
aload 0
invokespecial Message/<init>()V
aload 0
aload 1
putfield Producer_produce/receiver LProducer;
aload 0
aload 2
putfield Producer_produce/sender LActor;
return
.end method
.method public execute()V
.limit stack 1000
.limit locals 1000
aload 0
getfield Producer_produce/receiver LProducer;
aload 0
getfield Producer_produce/sender LActor;
invokevirtual Producer/produce(LActor;)V
return
.end method
