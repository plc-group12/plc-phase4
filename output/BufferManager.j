.class public BufferManager
.super Actor
.field producer LProducer;
.field consumer LConsumer;
.field empty Z
.field full Z
.field producerWaiting Z
.field consumerWaiting Z
.field bufferlenght I
.field nextProduce I
.field nextConsume I
.method public <init>(I)V
.limit stack 1000
.limit locals 1000
aload 0
iload 1
invokespecial Actor/<init>(I)V
return
.end method
.method public initial()V
.limit stack 1000
.limit locals 1000
bipush 2
aload 0
dup_x1
pop
putfield BufferManager/bufferlenght I
ldc 1
aload 0
dup_x1
pop
putfield BufferManager/empty Z
ldc 0
aload 0
dup_x1
pop
putfield BufferManager/full Z
ldc 0
aload 0
dup_x1
pop
putfield BufferManager/producerWaiting Z
ldc 0
aload 0
dup_x1
pop
putfield BufferManager/consumerWaiting Z
bipush 0
aload 0
dup_x1
pop
putfield BufferManager/nextProduce I
bipush 0
aload 0
dup_x1
pop
putfield BufferManager/nextConsume I
return
.end method
.method public setKnownActors(LProducer;LConsumer;)V
.limit stack 1000
.limit locals 1000
aload 0
aload 1
putfield BufferManager/producer LProducer;
aload 0
aload 2
putfield BufferManager/consumer LConsumer;
return
.end method
.method public send_giveMeNextProduce(LActor;)V
.limit stack 1000
.limit locals 1000
aload 0
new BufferManager_giveMeNextProduce
dup
aload 0
aload 1
invokespecial BufferManager_giveMeNextProduce/<init>(LBufferManager;LActor;)V
invokevirtual BufferManager/send(LMessage;)V
return
.end method
.method public giveMeNextProduce(LActor;)V
.limit stack 1000
.limit locals 1000
aload 0
getfield BufferManager/full Z
ldc 1
ixor
ifeq LABEL1
aload 0
getfield BufferManager/producer LProducer;
aload 0
invokevirtual Producer/send_produce(LActor;)V
goto LABEL2
LABEL1: 
ldc 1
aload 0
dup_x1
pop
putfield BufferManager/producerWaiting Z
LABEL2: 
return
.end method
.method public send_giveMeNextConsume(LActor;)V
.limit stack 1000
.limit locals 1000
aload 0
new BufferManager_giveMeNextConsume
dup
aload 0
aload 1
invokespecial BufferManager_giveMeNextConsume/<init>(LBufferManager;LActor;)V
invokevirtual BufferManager/send(LMessage;)V
return
.end method
.method public giveMeNextConsume(LActor;)V
.limit stack 1000
.limit locals 1000
aload 0
getfield BufferManager/empty Z
ldc 1
ixor
ifeq LABEL3
aload 0
getfield BufferManager/consumer LConsumer;
aload 0
invokevirtual Consumer/send_consume(LActor;)V
goto LABEL4
LABEL3: 
ldc 1
aload 0
dup_x1
pop
putfield BufferManager/consumerWaiting Z
LABEL4: 
return
.end method
.method public send_ackProduce(LActor;)V
.limit stack 1000
.limit locals 1000
aload 0
new BufferManager_ackProduce
dup
aload 0
aload 1
invokespecial BufferManager_ackProduce/<init>(LBufferManager;LActor;)V
invokevirtual BufferManager/send(LMessage;)V
return
.end method
.method public ackProduce(LActor;)V
.limit stack 1000
.limit locals 1000
aload 0
getfield BufferManager/nextProduce I
bipush 1
iadd
aload 0
getfield BufferManager/bufferlenght I
irem
aload 0
dup_x1
pop
putfield BufferManager/nextProduce I
aload 0
getfield BufferManager/nextProduce I
aload 0
getfield BufferManager/nextConsume I
if_icmpeq LABEL5
ldc 0
goto LABEL6
LABEL5:
ldc 1
LABEL6: 
ifeq LABEL7
ldc 1
aload 0
dup_x1
pop
putfield BufferManager/full Z
goto LABEL8
LABEL7: 
LABEL8: 
ldc 0
aload 0
dup_x1
pop
putfield BufferManager/empty Z
aload 0
getfield BufferManager/consumerWaiting Z
ifeq LABEL9
aload 0
getfield BufferManager/consumer LConsumer;
aload 0
invokevirtual Consumer/send_consume(LActor;)V
ldc 0
aload 0
dup_x1
pop
putfield BufferManager/consumerWaiting Z
goto LABEL10
LABEL9: 
LABEL10: 
return
.end method
.method public send_ackConsume(LActor;)V
.limit stack 1000
.limit locals 1000
aload 0
new BufferManager_ackConsume
dup
aload 0
aload 1
invokespecial BufferManager_ackConsume/<init>(LBufferManager;LActor;)V
invokevirtual BufferManager/send(LMessage;)V
return
.end method
.method public ackConsume(LActor;)V
.limit stack 1000
.limit locals 1000
aload 0
getfield BufferManager/nextConsume I
bipush 1
iadd
aload 0
getfield BufferManager/bufferlenght I
irem
aload 0
dup_x1
pop
putfield BufferManager/nextConsume I
aload 0
getfield BufferManager/nextConsume I
aload 0
getfield BufferManager/nextProduce I
if_icmpeq LABEL11
ldc 0
goto LABEL12
LABEL11:
ldc 1
LABEL12: 
ifeq LABEL13
ldc 1
aload 0
dup_x1
pop
putfield BufferManager/empty Z
goto LABEL14
LABEL13: 
LABEL14: 
ldc 0
aload 0
dup_x1
pop
putfield BufferManager/full Z
aload 0
getfield BufferManager/producerWaiting Z
ifeq LABEL15
aload 0
getfield BufferManager/producer LProducer;
aload 0
invokevirtual Producer/send_produce(LActor;)V
ldc 0
aload 0
dup_x1
pop
putfield BufferManager/producerWaiting Z
goto LABEL16
LABEL15: 
LABEL16: 
return
.end method
