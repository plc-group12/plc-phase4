.class public Consumer
.super Actor
.field buffer LBufferManager;
.method public <init>(I)V
.limit stack 1000
.limit locals 1000
aload 0
iload 1
invokespecial Actor/<init>(I)V
return
.end method
.method public initial()V
.limit stack 1000
.limit locals 1000
aload 0
aload 0
invokevirtual Consumer/send_beginConsume(LActor;)V
return
.end method
.method public setKnownActors(LBufferManager;)V
.limit stack 1000
.limit locals 1000
aload 0
aload 1
putfield Consumer/buffer LBufferManager;
return
.end method
.method public send_consume(LActor;)V
.limit stack 1000
.limit locals 1000
aload 0
new Consumer_consume
dup
aload 0
aload 1
invokespecial Consumer_consume/<init>(LConsumer;LActor;)V
invokevirtual Consumer/send(LMessage;)V
return
.end method
.method public consume(LActor;)V
.limit stack 1000
.limit locals 1000
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "consumer is consuming"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
getfield Consumer/buffer LBufferManager;
aload 0
invokevirtual BufferManager/send_ackConsume(LActor;)V
aload 0
aload 0
invokevirtual Consumer/send_beginConsume(LActor;)V
return
.end method
.method public send_beginConsume(LActor;)V
.limit stack 1000
.limit locals 1000
aload 0
new Consumer_beginConsume
dup
aload 0
aload 1
invokespecial Consumer_beginConsume/<init>(LConsumer;LActor;)V
invokevirtual Consumer/send(LMessage;)V
return
.end method
.method public beginConsume(LActor;)V
.limit stack 1000
.limit locals 1000
aload 0
getfield Consumer/buffer LBufferManager;
aload 0
invokevirtual BufferManager/send_giveMeNextConsume(LActor;)V
return
.end method
